# OpenClassrooms : Développeur d'Application Python

## Projet 2 : Utilisez les bases de Python pour l'analyse de marché

## Web Scraping Project: Books.toscrape.com

## Overview

This Python project is designed for web scraping data from the
website [Books.toscrape.com](https://books.toscrape.com/). It provides a Python script that extracts information about
books available on the site, such as titles, prices, ratings, and more.

## Features

- Scrapes book data from [Books.toscrape.com](https://books.toscrape.com/)
- Extracts information like book title, price, rating, and more
- Stores the scraped data in a structured format (CSV)

## Requirements

- beautifulsoup4==4.12.2
- certifi==2023.7.22
- charset-normalizer==3.2.0
- idna==3.4
- requests==2.31.0
- soupsieve==2.5
- urllib3==2.0.5

Python 3.x is required to run this project.

## Installation

Clone the repository:

```bash
git clone git@gitlab.com:openclassrooms_devapppython/oc_dap_p2.git
cd oc_dap_p2
```

```bash
pip install -r requirements.txt
```

## Usage

```bash
python script.py
```

## Data format

```python
headers = ['product_page_url', 'universal_product_code', 'title', 'price_including_tax', 'price_excluding_tax',
           'number_available', 'product_description', 'category', 'review_rating', 'image_url']
```

## License

This project is licensed under the MIT License

