import csv
import os
import re
import shutil

import requests
from bs4 import BeautifulSoup

baseUrl = "https://books.toscrape.com/"
category_url = "https://books.toscrape.com/catalogue/category/books/travel_2/index.html"
headers = ['product_page_url', 'universal_product_code', 'title', 'price_including_tax', 'price_excluding_tax',
           'number_available', 'product_description', 'category', 'review_rating', 'image_url']


def get_number(string):
    """
    This function is used to get the number from a string
    :param string: A number written in letters, representing the rating of a book between 1 and 5
    :return: The review rating of the book in number
    """
    match string:
        case 'One':
            return 1
        case 'Two':
            return 2
        case 'Three':
            return 3
        case 'Four':
            return 4
        case 'Five':
            return 5
        case _:
            return 0


def modify_last_part_of_url(url, new_page):
    """
    This function is used to modify the last part of an url
    :param url: actual url of the page we're scrapping
    :param new_page: the uri of the next page
    :return: modified url of the next page
    """
    url = url.split('/')
    url[len(url) - 1] = new_page
    return '/'.join(url)


def scrap_book_image(image_url, book_product_code):
    print(f"        🖼️ Scraping {book_product_code}.jpg...")
    image = requests.get(image_url, stream=True)
    with open(f"data/images/{book_product_code}.jpg", 'wb') as file:
        image.raw.decode_content = True
        shutil.copyfileobj(image.raw, file)
    return


def create_folder_if_not_existing():
    """
    This function is used to create a folder if it doesn't exist
    :return:
    """
    if not os.path.isdir('data'):
        os.mkdir('data')
    if not os.path.isdir('data/images'):
        os.mkdir('data/images')
    if not os.path.isdir('data/csv'):
        os.mkdir('data/csv')
    return


def export_to_csv(file_title, flags, data):
    """
    This function is used to export data to a csv file
    :param file_title: title of the file
    :param flags: flags used to open the file
    :param data: data to export
    :return: nothing
    """
    print(f"📁 Exporting {file_title} to csv...")
    with open(f'data/csv/{file_title}.csv', f"{flags}") as file:
        writer = csv.writer(file, delimiter=',')
        writer.writerow(headers)
        for row in data:
            writer.writerow(row)
    print(f"✅ {file_title} has been exported successfully !")


def scrap_book_by_url(book_url, step, length):
    """
    This function is used to scrap a book by its url
    :param length:
    :param step:
    :param book_url: url of the book we want to scrap
    :return: a row of data about the book already sorted
    """
    page = requests.get(book_url)
    soup = BeautifulSoup(page.content, 'html.parser')

    table = soup.select('.table td')

    print(f"    📖 Scraping {soup.find('h1').getText()}... ({step}/{length})")
    try:
        description = soup.select_one('#product_description ~ p').getText()
    except AttributeError:
        description = 'No description available'

    scrap_book_image(baseUrl + soup.select_one('.item.active > img')['src'].split('../../')[1], table[0].getText())

    return [
        book_url,
        table[0].getText(),
        soup.find('h1').getText(),
        table[2].getText(),
        table[3].getText(),
        int(re.search(r'\d+', soup.select_one('.availability').getText()).group()),
        description,
        soup.select('.breadcrumb > li')[2].getText().split('\n')[1],
        get_number(soup.select_one('.star-rating')['class'][1]),
        baseUrl + soup.select_one('.item.active > img')['src'].split('../../')[1],
    ]


def scrap_all_books_from_one_category(category_url, category_title, step, length):
    """
    This function is used to scrap all books from one category
    :param length: number of categories
    :param step: category number
    :param category_url: url of the category we want to scrap
    :param category_title: title of the category we want to scrap
    :return: nothing
    """
    print(f"🗃️ Now scraping {category_title}... ({step}/{length})")
    links_list = []
    data_list = []
    url = category_url
    j = 0
    while True:
        page = requests.get(url)
        soup = BeautifulSoup(page.content, 'html.parser')

        for book in soup.select('ol.row > li > article > h3 > a'):
            links_list.append(baseUrl + 'catalogue/' + book['href'].split('../../../')[1])

        if soup.select('.next > a'):
            url = modify_last_part_of_url(url, soup.select_one('.next > a')['href'])
        else:
            for link in links_list:
                j += 1
                data_list.append(scrap_book_by_url(link, j, len(links_list)))
            print(f"✅ {category_title} has been scraped successfully !")
            export_to_csv(category_title, 'w', data_list)
            return


def scrap_all_categories():
    """
    This function is used to scrap all categories
    :return: nothing
    """
    page = requests.get(baseUrl)
    soup = BeautifulSoup(page.content, 'html.parser')
    i = 0
    for category in soup.select('.nav-list > li > ul > li > a'):
        i += 1
        scrap_all_books_from_one_category(baseUrl + category['href'],
                                          category['href'].split('/')[len(category['href'].split('/')) - 2].split('_')[
                                              0].lower(), i, len(soup.select('.nav-list > li > ul > li > a')))


create_folder_if_not_existing()
scrap_all_categories()
